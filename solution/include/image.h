#include <stdint.h>
#include <stdlib.h>
#ifndef IMAGE_H
#define IMAGE_H

struct pixel { uint8_t b, g, r; };

struct image {
	uint64_t width, height;
	struct pixel* data;
};

enum image_status {
	CREATE_OK = 0,
	CREATE_ERROR = 1
};

struct image create_image(uint64_t width, uint64_t height);

void clear_image(struct image* const img);

#endif
