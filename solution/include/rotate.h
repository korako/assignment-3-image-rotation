#include "../include/image.h"
#ifndef ROTATE_H
#define ROTATE_H

struct image rotate(const struct image* img);

#endif
