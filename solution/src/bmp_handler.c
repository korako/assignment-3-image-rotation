#include "../include/image.h"
#include "../include/bmp.h"
#define TYPE 0x4D42
#define RESERVED 0
#define HEADER_SIZE 40
#define PLANES 1
#define BIT_COUNT 24
#define COMPRESSION 0
#define X_PELS_PER_METER 0
#define Y_PELS_PER_METER 0
#define COLOR_USER 0
#define COLOR_IMPORTANT 0
#define MULTIPLE 4

enum read_status from_bmp(FILE* in, struct image* img) {
	struct bmp_header header = {0};
	enum read_status status = read_header(in, &header);
	if (status == READ_INVALID_HEADER) return status;
	*img = create_image(header.biWidth, header.biHeight);
    if (img->width == 0 & img->height == 0) return INVALID_ALLOCATION;
	return read_pixels(in, img);
}

enum write_status to_bmp(FILE* out, struct image* img) {
	struct bmp_header header = create_header(img);
    size_t padding = get_padding(img->width);
	uint32_t zero = 0;
	if (fwrite(&header, sizeof(header), 1, out) != 1) return WRITE_HEADER_ERROR;
	for (size_t i = 0; i < img->height; i++) {
		if (fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out) != img->width) return WRITE_PIXELS_ERROR;
		if (fwrite(&zero, 1, padding, out) != padding) return WRITE_PADDING_ERROR;
	}
	return WRITE_OK;
}

enum read_status read_header(FILE* const in, struct bmp_header* const header){
    if (fread(header, sizeof(struct bmp_header), 1, in) == 0) return READ_INVALID_HEADER;
    if (TYPE  != header->bfType) return READ_INVALID_SIGNATURE;
    return READ_OK;
	
}

enum read_status read_pixels(FILE* in, struct image* img) {
    for (size_t i = 0; i < img->height; i++) {
            if (fread(img->data + (i * img->width), sizeof(struct pixel), img->width, in) != img->width) {
                free(img->data);
                return READ_INVALID_BITS;
            }
            if (fseek(in, get_padding(img->width), SEEK_CUR) != 0) {
                free(img->data);
                return READ_INVALID_BITS;
            }
        }
    return READ_OK;
}

struct bmp_header create_header(struct image* const img) {
	uint32_t size_of_picture = (sizeof(struct pixel) * img->width + get_padding(img->width))*img->height;
	struct bmp_header header = {
	.bfType = TYPE,
    .bfileSize =  sizeof(struct bmp_header) + size_of_picture, 
	.bfReserved =  RESERVED,
    .bOffBits = sizeof(struct bmp_header),
    .biSize = HEADER_SIZE,
    .biWidth = img->width,
    .biHeight = img->height,
    .biPlanes = PLANES, 
    .biBitCount = BIT_COUNT,
    .biCompression = COMPRESSION,
    .biSizeImage = size_of_picture,
    .biXPelsPerMeter = X_PELS_PER_METER,
    .biYPelsPerMeter = Y_PELS_PER_METER,
    .biClrUsed = COLOR_USER,
    .biClrImportant = COLOR_IMPORTANT,
	};
	return header;
}


uint8_t get_padding(uint32_t width) {
	return (MULTIPLE - (width * sizeof(struct pixel) % MULTIPLE)) % MULTIPLE;
}
