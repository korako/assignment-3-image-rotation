#include "../include/image.h"

struct image create_image(uint64_t width, uint64_t height) {
    struct pixel* data = malloc(sizeof(struct pixel) * width * height);
    struct image image = (struct image) {.width = width, .height = height, .data = data};
    return image;
} 

void clear_image(struct image* const img) {
    free(img->data);
    img->width = 0;
    img->height = 0;
}
