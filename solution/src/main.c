#include "../include/image.h"
#include "../include/bmp.h"
#include "../include/rotate.h"
#include <stdio.h>

int main(int argc, char** argv) {
    if (argc != 3) {
        fprintf(stderr, "%s", "Incorrect input format. Enter in the format: <source-image> <transformed-image>");
        return 1;
    }
    struct image img = {0};
    FILE* in = fopen(argv[1], "r");
    if (in == NULL) {
        fprintf(stderr, "%s", "First file opening error");
        return 1;
    }
    enum read_status read_status = from_bmp(in, &img);
    if (read_status != READ_OK) {
        clear_image(&img);
        fprintf(stderr, "%s", "File reading error");
        fclose(in);
        return 1;
    }
    if (fclose(in) == 0) {
        FILE* out = fopen(argv[2], "w");
        if (out == NULL) {
            fprintf(stderr, "%s", "Second file opening error");
            clear_image(&img);
            return 1;
        }
        struct image new_image = rotate(&img);
        enum write_status write_status = to_bmp(out, &new_image);
        if (write_status != WRITE_OK) {
            fprintf(stderr, "%s", "Error writing to the second file");
        }
        if (fclose(out) != 0) {
            fprintf(stderr, "%s", "Error closing the second file");
            clear_image(&img);
            clear_image(&new_image);
            return 1;
        }
        clear_image(&img);
        clear_image(&new_image);
        return 0;
    }
    else {
        fprintf(stderr, "%s", "Error closing the first file");
        clear_image(&img);
        return 1;
    }
}
