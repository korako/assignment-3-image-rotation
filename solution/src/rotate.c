#include "../include/rotate.h"

struct image rotate(const struct image* img) {
    struct image new_image = create_image(img->height, img->width);
    for (size_t i = 0; i < img->height; i++) {
        for (size_t j = 0; j < img->width; j++) {
            struct pixel old_pixel = img->data[i * img->width + j];
            new_image.data[(img->height - i - 1) + img->height * j] = old_pixel;
        }
    }
    return new_image;
}
